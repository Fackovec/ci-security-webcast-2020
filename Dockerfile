# Use an old .0 Docker image
FROM debian:10.0

# Install the build tools
RUN apt-get update && apt-get install -y build-essential
