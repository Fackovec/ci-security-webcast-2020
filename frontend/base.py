import django
from django.conf import settings

class FrontendServer(object):
    # Initiate AWS helpers
    def __init__(self):
        self.aws_key = "AKIA1234567890ABCDEF"

    def get_key(self):
        return self.aws_key

    def run(self):
        print("Hello from your GitLab Application Security Frontend Server!")

if __name__ == '__main__':
    fs = FrontendServer()
    fs.run()


